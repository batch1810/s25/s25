//Mini Activity:

/*
Create a fruits collection and insert five documents with the following fields and values:

    name: Apple
    color: Red
    supplier_id:1
    stocks: 20
    price: 40
    onSale: true
    origin: Philippines & US

    name: Banana
    color: Yellow
    stocks: 15
    price: 20
    supplier_id: 2
    onSale: true
    origin: Philippines, Ecuador

    name: Kiwi
    color: Green
    stocks: 25
    price: 50
    supplier_id: 1
    onSale: true
    origin: US, China

    name: Mango
    color: Yellow
    stocks: 10
    price: 120
    supplier_id: 2
    onSale: false
    origin: Philippines, India


Get all the inserted documents in the fruits collection by using find() method and take a screenshot of the result and send it at the batch hangouts.

*/

//[Section] MongoDB Aggregation
/*
    This is the act or the process of generating manipulated data and perform operations to create filtered results that help in data analysis.

    This helps us in creating reports from analyzing the data provided in our documents.

    This helps us arrive at summarized information not previously shown in our documents.
*/

//Using the aggregate method
/*
    Aggregation is done by steps/stages. Each stage/step is called a pipeline

    The "$" symbol will refer to a field name that is available in the documents that are being aggregated on.

    $match -> used to pass the documents that meet the specified condition(s) to the next pipeline.
    Syntax:
        {$match: {"field": "value"}}

    $group -> used to group elements/documents together and create an analysis of these
        {$group: {"_id": "value", "fieldResult": "valueResult"}}

    
    Syntax:
        db.collectionName.aggregate([
            {$match: {"field": "value"}},
            {$group: {"_id": "$field", "result": {"operation"}}}
        ])
*/
db.fruits.aggregate([
    {$match: {"onSale": true}},
    {$group: {"_id": "$supplier_id", "total": {$sum: "$stocks"}}}
]);

//Field projection with aggregation
/*
    $project -> used when aggregating data to include/exclude fields from the returned results.

    Syntax:
        {$project: {"field": 1/0}}
*/
db.fruits.aggregate([
    {$match: {"onSale": true}},
    {$group: {"_id": "$supplier_id", "total": {$sum: "$stocks"}}},
    {$project: {"_id": 0}}
]);

//Sorting aggregated results
/*
    $sort
    -> used to change the order of aggregated results
    -> providing a value of -1 will sort the aggregated results in a reverse order

    Syntax: {$sort": {"field": 1/-1}}
*/
db.fruits.aggregate([
    {$match: {"onSale": true}},
    {$group: {"_id": "$supplier_id", "total": {$sum: "$stocks"}}},
    {$sort: {"total": -1}}
]);

//Aggregating results based on array fields
/*
    $unwind -> deconstructs an array field

    Syntax: {$unwind: "field"}
*/
db.fruits.aggregate([
    {$unwind: "$origin"}
]);

//Display fruit documents by their origin and the kinds of fruits that are supplied
db.fruits.aggregate([
    {$unwind: "$origin"},
    {$group: {"_id": "$origin", "kinds": {$sum: 1}}}
]);
